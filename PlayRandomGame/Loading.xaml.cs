﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Entity;
using System.Linq;
using SteamWin;
using System.Threading;
using System.ComponentModel;

namespace PlayRandomGame
{
    /// <summary>
    /// Логика взаимодействия для Loading.xaml
    /// </summary>
    public partial class Loading : Window
    {
        GamesDBContainer allGames = new GamesDBContainer();
        SteamGamesList gamesList = new SteamGamesList();
        SteamUser user;
        BackgroundWorker worker;
        bool isCancel = false;
        public Loading()
        {
            InitializeComponent();
            worker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            worker.DoWork += fillGamesBaseWorker;
            worker.ProgressChanged += fillGamesBaseWorker_Progress;
            worker.RunWorkerCompleted += fillGamesBaseWorker_Completed;

            user = new SteamUser("princetails");
            if (user != null && user.id64 > 0)
                gamesList.gamesList = SteamGamesList.getGamesList(user);
            progressBar.Maximum = gamesList.gamesList.Count;
            progressLabel.Content = string.Format("{0} from {1}", progressBar.Value, progressBar.Maximum);

        }

        private void fillGamesBaseWorker_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            if (isCancel)
            {
                this.DialogResult = false;
                this.Close();
                return;
            }

            this.DialogResult = true;
            this.Close();
        }

        private void fillGamesBaseWorker_Progress(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value++;
            progressLabel.Content = string.Format("{0} from {1}", progressBar.Value, progressBar.Maximum);
        }

        private void fillGamesBaseWorker(object sender, DoWorkEventArgs e)
        {

            Game newGame = new Game();
            List<Game> newGameList = new List<Game>();
            //bool dbChanged = false;

            foreach (SteamGame game in gamesList.gamesList)
            {
                if (isCancel)
                    return;
                if (allGames.GameSet.AsNoTracking().Where(find => find.SteamId == game.Id).FirstOrDefault() != null)
                {
                    bool changed = false;
                    var changedGame = allGames.GameSet.AsNoTracking().Where(find => find.SteamId == game.Id).FirstOrDefault();
                    if (changedGame.ApplicationType == "" || changedGame.ApplicationType == "Unknowned" || changedGame.ApplicationType == null)
                    {
                        changed = true;
                        changedGame.ApplicationType = SteamDB.getType(changedGame.SteamId);
                    }
                    if (changedGame.LogoURL == null || changedGame.LogoURL == "")
                    {
                        changed = true;
                        changedGame.LogoURL = game.LogoURL;
                    }
                    if (changedGame.ApplicationCategory == "" || changedGame.ApplicationCategory == null)
                    {
                        changed = true;
                        var cat = SteamDB.getCategory(changedGame.SteamId);
                        string c = "";
                        foreach (string ct in game.ApplicationCategory)
                            c += ct + "|";
                        changedGame.ApplicationCategory = c;
                    }
                    if(changed)
                    {
                        allGames.GameSet.Attach(changedGame);
                        allGames.Entry(changedGame).State = EntityState.Modified;
                        allGames.SaveChanges();

                    }
                    worker.ReportProgress(1);
                    continue;
                }
               
                //dbChanged = true;
                game.ApplicationCategory = SteamDB.getCategory(game.Id);
                game.ApplicationType = SteamDB.getType(game.Id);
                //game.IconURL = SteamDB.getUrlIcon(game.Id);
                game.HowLongToBeat = new SteamWin.HLTB(game.Name);


                newGame.SteamId = game.Id;
                newGame.Name = game.Name;
                newGame.ApplicationType = game.ApplicationType;
                //newGame.IconURL = game.IconURL;
                newGame.LogoURL = game.LogoURL;

                string category = "";
                foreach (string cat in game.ApplicationCategory)
                    category += cat + "|";
                newGame.ApplicationCategory = category;


                HLTB newHLTB = new HLTB();
                newHLTB.SteamID = game.Id;
                newHLTB.Name = game.Name;
                newHLTB.Main = game.HowLongToBeat.Main;
                newHLTB.MainExtra = game.HowLongToBeat.MainExtra;
                newHLTB.Completionist = game.HowLongToBeat.Completionist;
                newHLTB.GameLink = game.HowLongToBeat.GameLink;
                //newGameList.Add(newGame);
                allGames.Entry(newHLTB).State = EntityState.Added;

                ////allGames.HLTBSet.Attach(newHLTB);
                allGames.HLTBSet.Add(newHLTB);

                allGames.Entry(newGame).State = EntityState.Added;

                allGames.GameSet.Add(newGame);
                allGames.SaveChanges();

                //if (gamesList.gamesList.IndexOf(game) > 1)
                //    break;
                worker.ReportProgress(1);

            }
            //allGames.GamesSet.AddRange(newGameList);

            //if (dbChanged)
            //    allGames.SaveChanges();
        }

        private void LoadingWindow_Loaded(object sender, RoutedEventArgs e)
        {
            worker.RunWorkerAsync();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            isCancel = true;
            Cancel.IsEnabled = false;
        }

        private void LoadingWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
