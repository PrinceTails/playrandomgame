
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/23/2019 17:55:40
-- Generated from EDMX file: D:\Projects\PlayRandomGame\PlayRandomGame\GamesDB.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [GamesDataBase];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[InstalledSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[InstalledSet];
GO
IF OBJECT_ID(N'[dbo].[NotInterestingSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NotInterestingSet];
GO
IF OBJECT_ID(N'[dbo].[HLTBSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HLTBSet];
GO
IF OBJECT_ID(N'[dbo].[GameSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GameSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'InstalledSet'
CREATE TABLE [dbo].[InstalledSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SteamId] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [SizeOnDisk] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'NotInterestingSet'
CREATE TABLE [dbo].[NotInterestingSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SteamId] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'HLTBSet'
CREATE TABLE [dbo].[HLTBSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SteamID] int  NOT NULL,
    [Main] nvarchar(max)  NULL,
    [MainExtra] nvarchar(max)  NULL,
    [Completionist] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL,
    [GameLink] nvarchar(max)  NULL
);
GO

-- Creating table 'GameSet'
CREATE TABLE [dbo].[GameSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SteamId] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [IconURL] nvarchar(max)  NULL,
    [LogoURL] nvarchar(max)  NULL,
    [ApplicationType] nvarchar(max)  NULL,
    [ApplicationCategory] nvarchar(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'InstalledSet'
ALTER TABLE [dbo].[InstalledSet]
ADD CONSTRAINT [PK_InstalledSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'NotInterestingSet'
ALTER TABLE [dbo].[NotInterestingSet]
ADD CONSTRAINT [PK_NotInterestingSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HLTBSet'
ALTER TABLE [dbo].[HLTBSet]
ADD CONSTRAINT [PK_HLTBSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GameSet'
ALTER TABLE [dbo].[GameSet]
ADD CONSTRAINT [PK_GameSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------