﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Youtube = YoutubeSearch;

namespace PlayRandomGame
{
    /// <summary>
    /// Логика взаимодействия для Gameplay.xaml
    /// </summary>
    public partial class Gameplay : Window
    {
        const string youtubeEmbed = "<body><iframe width='640' height='480'  src='https://www.youtube.com/embed/{0}?rel=0&autoplay=1' frameborder='0' allowfullscreen></iframe><body>";
        List<Youtube.VideoInformation> youtubeSearch = new List<Youtube.VideoInformation>();
        public Gameplay(string name)
        {
            InitializeComponent();
            youtubeSearch = new Youtube.VideoSearch().SearchQuery(name + " official Gameplay", 1);
            youtubeBrowser.NavigateToString(String.Format(youtubeEmbed, youtubeSearch[0].Url.Replace("http://www.youtube.com/watch?v=", "")));


        }

        private void Video_button_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Label label = (Label)e.Source;
            string number = label.Content.ToString().Substring(label.Content.ToString().Length-1);
            switch(number)
            {
                case "1":
                    {
                        youtubeBrowser.NavigateToString(String.Format(youtubeEmbed, youtubeSearch[0].Url.Replace("http://www.youtube.com/watch?v=", "")));
                        break;
                    }
                case "2":
                    {
                        youtubeBrowser.NavigateToString(String.Format(youtubeEmbed, youtubeSearch[1].Url.Replace("http://www.youtube.com/watch?v=", "")));

                        break;
                    }
                case "3":
                    {
                        youtubeBrowser.NavigateToString(String.Format(youtubeEmbed, youtubeSearch[2].Url.Replace("http://www.youtube.com/watch?v=", "")));

                        break;
                    }
            }
        }

        private void Video1_button_MouseEnter(object sender, MouseEventArgs e)
        {
        }

        private void Video1_button_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void Gameplay_Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            youtubeBrowser.Dispose();
        }
    }
}
