﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using SteamWin;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using Youtube=YoutubeSearch;
using Microsoft.Win32;
using System.Data.Entity;
using System.Linq;

namespace PlayRandomGame
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SteamApp steamapp;
        SteamGame game;
        SteamGamesList sgl;
        SteamGamesList currentGL;
        AppSettings appSet;
        string[] filter;
        GamesDBContainer contex = new GamesDBContainer();
        bool currentGameIsInstalled = false;
        public MainWindow()
        {

            SetBrowserFeatureControl();
            appSet = new AppSettings();
            InitializeComponent();
            initSettings();
            steamapp = new SteamApp();
            sgl = new SteamGamesList();
            currentGL = new SteamGamesList();

            Loading loading = new Loading();
            if(loading.ShowDialog()==false)
            {
                MessageBox.Show("Loading canceled");
                this.Close();
                return;
            }

            filter = new string[9];
            updateInstalledGames();

            if (appSet.Installed)
                random();
            else
                allRandom();
        }

        private void allRandom()
        {
            Game gameDB = new Game();
            var dbGamesList = contex.GameSet.AsNoTracking().Where(x=>x.ApplicationType=="Game").ToList();
            if (dbGamesList.Count == 0)
                return;
            List<Game> filteredList = new List<Game>();

            if (appSet.needfilter)
            {
                List<string> selectedCategory = getSelectedCategory();
                if (selectedCategory == null)
                {
                    //something go wrong
                }
                string categoryFilter = selectedCategory[0];
                filteredList = contex.GameSet.AsNoTracking().Where(x => x.ApplicationCategory.Contains(categoryFilter)).ToList();
                for (int i = 1; i < selectedCategory.Count; i++)
                {
                    categoryFilter = selectedCategory[i];
                    filteredList = filteredList.Where(x => x.ApplicationCategory.Contains(categoryFilter)).ToList();
                }
                if (filteredList.Count == 0)
                {
                    MessageBox.Show("Игор по таким категориям нет");
                    return;
                }
            }

            while (true)
            {
                Random rnd = new Random();
                 gameDB = dbGamesList[rnd.Next(0, dbGamesList.Count)];
                if (dbGamesList.Count == 0)
                {
                    MessageBox.Show("Игор по таким категориям нет");
                    return;
                }
                if (contex.NotInterestingSet.AsNoTracking().Where(x => x.SteamId == gameDB.SteamId).FirstOrDefault() == null)
                {
                    if (appSet.needfilter)
                        if (filteredList.Where(x => x.SteamId == gameDB.SteamId).FirstOrDefault() == null)
                        {
                            dbGamesList.Remove(gameDB);
                            continue;
                        }
                    break;
                }
            }

            game = new SteamGame();
            game.Name = gameDB.Name;
            game.Id = gameDB.SteamId;
            game.LogoURL = gameDB.LogoURL;

            if (appSet.needHltb)
            {
                var hltbForGame = contex.HLTBSet.AsNoTracking().Where(x => x.SteamID == game.Id).FirstOrDefault();
                if(hltbForGame==null)
                {
                    //get and write to hltbDB
                }
                hltbLabel.Content = "How Long To Beat: " + hltbForGame.Main;
                hltbLabel.ToolTip = "Main + Extra: " + hltbForGame.MainExtra + "\nCompletionist: " + hltbForGame.Completionist;
                hltbPanel.Visibility = Visibility.Visible;
                game.HowLongToBeat = new SteamWin.HLTB(hltbForGame.Name, hltbForGame.SteamID.ToString(), hltbForGame.Main, 
                    hltbForGame.MainExtra, hltbForGame.Completionist, hltbForGame.GameLink);
            }
            if (contex.InstalledSet.AsNoTracking().Where(x => x.SteamId == game.Id).FirstOrDefault() == null)
            {
                play_image.ToolTip = "Install " + game.Name;
                currentGameIsInstalled = false;
            }
            else
            {
                play_image.ToolTip = "Play " + game.Name;
                currentGameIsInstalled = true;
            }

            if(game.LogoURL!=null)
                gameImage.Source = BitmapFrame.Create(new Uri(game.LogoURL));
        }
        private void random()
        {
            if (currentGL.gamesList.Count == 0)
                return;

            List<Installed> installedList = contex.InstalledSet.ToList();
            List<Game> filteredList = new List<Game>();
            if (appSet.needfilter)
            {
                List<string> selectedCategory = getSelectedCategory();
                if (selectedCategory == null)
                {
                    //something go wrong
                }
                string categoryFilter = selectedCategory[0];
                filteredList = contex.GameSet.AsNoTracking().Where(x => x.ApplicationCategory.Contains(categoryFilter)).ToList();
                for (int i = 1; i < selectedCategory.Count; i++)
                {
                    categoryFilter = selectedCategory[i];
                    filteredList = filteredList.Where(x => x.ApplicationCategory.Contains(categoryFilter)).ToList();
                }
                if (filteredList.Count == 0)
                {
                    MessageBox.Show("Игор по таким категориям нет");
                    return;
                }
            }

            while (true)
            {
                Random rnd = new Random();
                if (installedList.Count == 0)
                {
                    MessageBox.Show("Игор по таким категориям нет");
                    return;
                }
                Installed selectedInstalledGame = installedList[rnd.Next(0, installedList.Count)];
                int steamid = selectedInstalledGame.SteamId;

                game = new SteamGame();
                if (contex.NotInterestingSet.AsNoTracking().Where(x => x.SteamId == selectedInstalledGame.SteamId).FirstOrDefault() == null)
                {
                    var selectedGame = contex.GameSet.AsNoTracking().Where(x => x.SteamId == selectedInstalledGame.SteamId).FirstOrDefault();
                    if (selectedGame == null)
                        continue;
                    if (appSet.needfilter)
                        if (filteredList.Where(x => x.SteamId == selectedGame.SteamId).FirstOrDefault() == null)
                        {
                            installedList.Remove(selectedInstalledGame);
                            continue;
                        }
                    game.Name = selectedGame.Name;
                    game.Id = selectedGame.SteamId;
                    game.SizeOnDisk = Convert.ToInt64(selectedInstalledGame.SizeOnDisk);
                    game.LogoURL = selectedGame.LogoURL;
                    game.IconURL = selectedGame.IconURL;

                    break;
                }

            }

            play_image.ToolTip = "Play " + game.Name;
            gameImage.Source = BitmapFrame.Create(new Uri(game.LogoURL));
            if (!appSet.needHltb)
                return;
            var selectedGameHLTB = contex.HLTBSet.AsNoTracking().AsNoTracking().Where(x => x.SteamID == game.Id).FirstOrDefault();
            if (selectedGameHLTB != null && selectedGameHLTB.GameLink != "Unknown")
            {
                hltbLabel.Content = "How Long To Beat: " + selectedGameHLTB.Main;
                hltbLabel.ToolTip = "Main + Extra: " + selectedGameHLTB.MainExtra + "\nCompletionist: " + selectedGameHLTB.Completionist;
                hltbPanel.Visibility = Visibility.Visible;
                game.HowLongToBeat = new SteamWin.HLTB(selectedGameHLTB.Name, selectedGameHLTB.SteamID.ToString(), selectedGameHLTB.Main,
    selectedGameHLTB.MainExtra, selectedGameHLTB.Completionist, selectedGameHLTB.GameLink);
            }
            else
            {
                hltbPanel.Visibility = Visibility.Hidden;
            }
        }
        private void writeGames()
        {
            contex.Database.ExecuteSqlCommand("TRUNCATE TABLE InstalledSet");
            contex.SaveChanges();

            for (int i = 0; i < currentGL.gamesList.Count; i++)
            {
                int steamId = currentGL.gamesList[i].Id;
                Game fromDBGame = contex.GameSet.AsNoTracking().Where(find => find.SteamId == steamId).FirstOrDefault();
                if(fromDBGame==null)
                    continue;

                Installed installedGame = new Installed();
                contex.Entry(installedGame).State = EntityState.Added;
                installedGame.Name = fromDBGame.Name;
                installedGame.SizeOnDisk = currentGL.gamesList[i].SizeOnDisk.ToString();
                installedGame.SteamId = fromDBGame.SteamId;
                contex.InstalledSet.Add(installedGame);
                contex.SaveChanges();
            }
        }

        private void initSettings()
        {
            if (File.Exists("settings.st"))
            {
                XmlSerializer ser = new XmlSerializer(typeof(AppSettings));
                TextReader reader = new StreamReader("settings.st");
                appSet = ser.Deserialize(reader) as AppSettings;
                reader.Close();

                checkboxApply();
                this.Focus();
            }
        }

        private void checkboxApply()
        {
            if (appSet.needfilter)
            {
                resetCheckBoxes();
                for (int j = 0; j < appSet.category.GetLength(0); j++)
                {
                    switch (appSet.category[j])
                    {
                        case "Multi-player":
                            cb_mp.IsChecked = true;
                            break;
                        case "Single-player":
                            cb_sp.IsChecked = true;
                            break;
                        case "Co-op":
                            cb_co.IsChecked = true;
                            break;
                        case "Shared/Split Screen":
                            cb_ss.IsChecked = true;
                            break;
                        case "Cross-Platform Multiplayer":
                            cb_cpmp.IsChecked = true;
                            break;
                        case "Online Multi-Player":
                            cb_omp.IsChecked = true;
                            break;
                        case "Local Multi-Player":
                            cb_lmp.IsChecked = true;
                            break;
                        case "Online Co-op":
                            cb_oco.IsChecked = true;
                            break;
                        case "Local Co-op":
                            cb_lco.IsChecked = true;
                            break;
                        default:
                            break;
                    }
                }

            }
            if (appSet.needHltb)
                cb_hltb.IsChecked = true;
            if (appSet.Installed)
                cb_installed.IsChecked = true;
        }

        private void resetCheckBoxes()
        {
            cb_all.IsChecked = false;
            cb_mp.IsChecked = false;
            cb_sp.IsChecked = false;
            cb_co.IsChecked = false;
            cb_ss.IsChecked = false;
            cb_cpmp.IsChecked = false;
            cb_omp.IsChecked = false;
            cb_lmp.IsChecked = false;
            cb_oco.IsChecked = false;
            cb_lco.IsChecked = false;
        }

        private void updateInstalledGames()
        {
            var instlist = sgl.getInstalledList(steamapp.LibraryFolders);
            sgl = new SteamGamesList();
            sgl.getInstalledSteamGames(instlist);

            if (sgl.gamesList.Count == 0)
                return;
            currentGL.gamesList = sgl.gamesList;
            writeGames();
        }

        

        private List<string> getSelectedCategory()
        {
            List<string> output = new List<string>();

            if (cb_co.IsChecked == true)
                output.Add("Co-op");
            if(cb_cpmp.IsChecked==true)
                output.Add("Cross-Platform Multiplayer");
            if(cb_lco.IsChecked==true)
                output.Add("Local Co-op");
            if (cb_lmp.IsChecked == true)
                output.Add("Local Multi-Player");
            if (cb_mp.IsChecked == true)
                output.Add("Multi-player");
            if (cb_oco.IsChecked == true)
                output.Add("Online Co-op");
            if (cb_omp.IsChecked == true)
                output.Add("Online Multi-Player");
            if (cb_sp.IsChecked == true)
                output.Add("Single-player");
            if (cb_ss.IsChecked == true)
                output.Add("Shared/Split Screen");

            return output;
        }

        #region visual buttons
        private void play_image_MouseEnter(object sender, MouseEventArgs e)
        {
            play_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/play_active.png"));
        }

        private void play_image_MouseLeave(object sender, MouseEventArgs e)
        {
            play_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/play_inactive.png"));

        }

        private void random_image_MouseEnter(object sender, MouseEventArgs e)
        {
            random_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/random_active.png"));

        }

        private void random_image_MouseLeave(object sender, MouseEventArgs e)
        {
            random_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/random_inactive.png"));

        }

        private void settings_image_MouseEnter(object sender, MouseEventArgs e)
        {
            settings_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/setting_active.png"));

        }

        private void settings_image_MouseLeave(object sender, MouseEventArgs e)
        {
            settings_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/setting_inactive.png"));

        }

        private void close_image_MouseEnter(object sender, MouseEventArgs e)
        {
            close_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/close_active.png"));

        }

        private void close_image_MouseLeave(object sender, MouseEventArgs e)
        {
            close_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/close_inactive.png"));

        }
        private void not_interesting_MouseEnter(object sender, MouseEventArgs e)
        {
            not_interesting_image.Source= new BitmapImage(new Uri("pack://application:,,,/Images/notinteresting_active.png"));
        }

        private void not_interesting_MouseLeave(object sender, MouseEventArgs e)
        {
            not_interesting_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/notinteresting_inactive.png"));
        }
        private void gameplay_image_MouseEnter(object sender, MouseEventArgs e)
        {
            gameplay_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/gameplay_active.png"));

        }

        private void gameplay_image_MouseLeave(object sender, MouseEventArgs e)
        {
            gameplay_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/gameplay_inactive.png"));

        }
        private void store_image_MouseEnter(object sender, MouseEventArgs e)
        {
            store_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/store_active.png"));

        }

        private void store_image_MouseLeave(object sender, MouseEventArgs e)
        {
            store_image.Source = new BitmapImage(new Uri("pack://application:,,,/Images/store_inactive.png"));

        }
        #endregion

        private void play_image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (currentGameIsInstalled)
                SteamGame.launchGame(game.Id);
            //else
            //    SteamGame.installGame(game.Id);
        }

        private void close_image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void random_image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (appSet.Installed)
                random();
            else
                allRandom();
        }

        private void settings_image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            category_filter_menu.Visibility = Visibility.Visible;
            category_filter_menu.IsOpen = true;
        }

        //
        //Придумать как переключать всё без проблем
        //
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (category_filter_menu.Items.Count < 12)
                return;
            CheckBox cb = (CheckBox)sender;
            if (cb.Content.ToString() == "All" && (bool)cb.IsChecked)
            {
                appSet.needfilter = false;
                CheckBox_Toogle((bool)cb.IsChecked);
            }
            else if (cb.Content.ToString() == "All" && !(bool)cb.IsChecked)
            {
                CheckBox_Toogle((bool)cb.IsChecked);
                appSet.needfilter = true;
            }
            else if (cb.Content.ToString() != "All")
            {
                CheckBox tmpAll = (CheckBox)category_filter_menu.Items[0];
                bool needToogleAll = true;
                for (int i = 2; i < 11; i++)
                {
                    CheckBox tmp = (CheckBox)category_filter_menu.Items[i];
                    if (!(bool)tmp.IsChecked)
                    {
                        needToogleAll = false;
                        tmpAll.IsChecked = false;
                        return;
                    }
                }
                if (needToogleAll && !(bool)tmpAll.IsChecked)
                {
                    tmpAll.IsChecked = true;
                }
            }
        }
        private void CheckBox_Toogle(bool flag)
        {
            if (flag)
            {

                for (int i = 2; i < 11; i++)
                {
                    CheckBox tmp = (CheckBox)category_filter_menu.Items[i];
                    if (tmp != null)
                        tmp.IsChecked = true;
                }
            }
            else
            {
                for (int i = 2; i < 11; i++)
                {
                    CheckBox tmp = (CheckBox)category_filter_menu.Items[i];
                    if (tmp != null)
                        tmp.IsChecked = false;
                }
            }
        }


        private void confirm_category_button_Click(object sender, RoutedEventArgs e)
        {
            if (appSet == null)
                appSet = new AppSettings();
            List<string> tmpcat = new List<string>();
            CheckBox cb = (CheckBox)category_filter_menu.Items[0];
            if ((bool)cb.IsChecked)
                appSet.needfilter = false;
            else
                appSet.needfilter = true;
            if (appSet.Installed)
                random();
            else
                allRandom();
        }

        private void Main_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            XmlSerializer ser = new XmlSerializer(typeof(AppSettings));
            TextWriter writer = new StreamWriter("settings.st");

            ser.Serialize(writer, appSet);
            writer.Close();
        }

        private void hltbLabel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(game.HowLongToBeat!=null && game.HowLongToBeat.GameLink!= "Unknown")
                System.Diagnostics.Process.Start(game.HowLongToBeat.GameLink);
        }

        private void cb_hltb_Checked(object sender, RoutedEventArgs e)
        {
            
        }

        private void SetBrowserFeatureControl()
        {
            // http://msdn.microsoft.com/en-us/library/ee330720(v=vs.85).aspx

            // FeatureControl settings are per-process
            var fileName = System.IO.Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);

            // make the control is not running inside Visual Studio Designer
            if (String.Compare(fileName, "devenv.exe", true) == 0 || String.Compare(fileName, "XDesProc.exe", true) == 0)
                return;

            SetBrowserFeatureControlKey("FEATURE_BROWSER_EMULATION", fileName, GetBrowserEmulationMode()); // Webpages containing standards-based !DOCTYPE directives are displayed in IE10 Standards mode.
            SetBrowserFeatureControlKey("FEATURE_AJAX_CONNECTIONEVENTS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_ENABLE_CLIPCHILDREN_OPTIMIZATION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_MANAGE_SCRIPT_CIRCULAR_REFS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_DOMSTORAGE ", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_GPU_RENDERING ", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_IVIEWOBJECTDRAW_DMLT9_WITH_GDI  ", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_DISABLE_LEGACY_COMPRESSION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_LOCALMACHINE_LOCKDOWN", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_OBJECT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_SCRIPT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_DISABLE_NAVIGATION_SOUNDS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_SCRIPTURL_MITIGATION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_SPELLCHECKING", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_STATUS_BAR_THROTTLING", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_TABBED_BROWSING", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_VALIDATE_NAVIGATE_URL", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_DOCUMENT_ZOOM", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_POPUPMANAGEMENT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_MOVESIZECHILD", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_ADDON_MANAGEMENT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_WEBSOCKET", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WINDOW_RESTRICTIONS ", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_XMLHTTP", fileName, 1);
        }

        private UInt32 GetBrowserEmulationMode()
        {
            int browserVersion = 7;
            using (var ieKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer",
                RegistryKeyPermissionCheck.ReadSubTree,
                System.Security.AccessControl.RegistryRights.QueryValues))
            {
                var version = ieKey.GetValue("svcVersion");
                if (null == version)
                {
                    version = ieKey.GetValue("Version");
                    if (null == version)
                        throw new ApplicationException("Microsoft Internet Explorer is required!");
                }
                int.TryParse(version.ToString().Split('.')[0], out browserVersion);
            }

            UInt32 mode = 11000; // Internet Explorer 11. Webpages containing standards-based !DOCTYPE directives are displayed in IE11 Standards mode. Default value for Internet Explorer 11.
            switch (browserVersion)
            {
                case 7:
                    mode = 7000; // Webpages containing standards-based !DOCTYPE directives are displayed in IE7 Standards mode. Default value for applications hosting the WebBrowser Control.
                    break;
                case 8:
                    mode = 8000; // Webpages containing standards-based !DOCTYPE directives are displayed in IE8 mode. Default value for Internet Explorer 8
                    break;
                case 9:
                    mode = 9000; // Internet Explorer 9. Webpages containing standards-based !DOCTYPE directives are displayed in IE9 mode. Default value for Internet Explorer 9.
                    break;
                case 10:
                    mode = 10000; // Internet Explorer 10. Webpages containing standards-based !DOCTYPE directives are displayed in IE10 mode. Default value for Internet Explorer 10.
                    break;
                default:
                    // use IE11 mode by default
                    break;
            }

            return mode;
        }
        private void SetBrowserFeatureControlKey(string feature, string appName, uint value)
        {
            using (var key = Registry.CurrentUser.CreateSubKey(
                String.Concat(@"Software\Microsoft\Internet Explorer\Main\FeatureControl\", feature),
                RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                key.SetValue(appName, (UInt32)value, RegistryValueKind.DWord);
            }
        }

        private void not_interesting_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var nig = contex.GameSet.AsNoTracking().Where(x => x.SteamId == game.Id).FirstOrDefault();
            NotInteresting g = new NotInteresting();
            g.Name = nig.Name;
            g.SteamId = nig.SteamId;
            contex.NotInterestingSet.Add(g);
            contex.SaveChanges();
            if (appSet.Installed)
                random();
            else
                allRandom();
        }

        private void store_image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Process.Start(string.Format(@"https://store.steampowered.com/app/{0}", game.Id));
        }

        private void gameplay_image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Gameplay gameplay = new Gameplay(game.Name);
            gameplay.Show();
        }

        private void cb_installed_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void cb_installed_Click(object sender, RoutedEventArgs e)
        {
            CheckBox temp = (CheckBox)e.Source;
            if (temp.IsChecked == true)
            {
                if (MessageBox.Show("Do you want use random for installed games?", "Accept changes!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    appSet.Installed = true;
                    //updateInstalledGames();
                    random();
                }
                else
                {
                    appSet.Installed = false;
                    allRandom();
                }
            }
            else
            {
                if (MessageBox.Show("Do you want use random for all games?", "Accept changes!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    appSet.Installed = false;
                    allRandom();
                }
                else
                {
                    appSet.Installed = true;
                    //updateInstalledGames();
                    random();
                }
            }
        }

        private void cb_hltb_Click(object sender, RoutedEventArgs e)
        {
            CheckBox tmp = (CheckBox)e.Source;
            if (tmp.IsChecked == true)
            {
                if (MessageBox.Show("Do you want get info from HLTB?", "Accept changes!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    appSet.needHltb = true;
                    if(game.HowLongToBeat!=null && game.HowLongToBeat.GameLink!="Unknown")
                        hltbPanel.Visibility = Visibility.Visible;

                }
                else
                {
                    appSet.needHltb = false;
                    hltbPanel.Visibility = Visibility.Hidden;
                }
            }
            else
            {
                if (MessageBox.Show("Do you want hide HLTB-information?", "Accept changes!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    appSet.needHltb = false;
                    hltbPanel.Visibility = Visibility.Hidden;
                }
                else
                {
                    if (game.HowLongToBeat != null && game.HowLongToBeat.GameLink != "Unknown")
                        hltbPanel.Visibility = Visibility.Visible;
                    appSet.needHltb = true;
                }
            }
        }
    }
}
