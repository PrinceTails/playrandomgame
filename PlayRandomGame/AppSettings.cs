﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayRandomGame
{
    public class AppSettings
    {
        public bool needfilter;
        public bool needHltb;
        public string[] category;
        public bool Installed;
        public DateTime LastUpdatedHLTB;

        public AppSettings()
        {
            needfilter = false;
            needHltb = false;
            category = null;
            Installed = false;
        }
        public AppSettings(bool nf, bool hltb ,string [] f, bool inst)
        {
            needfilter = nf;
            needHltb = hltb;
            Installed = inst;
            category = f;
        }
    }
}
